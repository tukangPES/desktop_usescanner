﻿Option Strict Off

Imports clsScanner
Imports clsScanner.TwainLib

Public Class Form1
    Implements IMessageFilter

    Private _theTwain As New TwainLib.Twain
    Private _msgfilter As Boolean

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        If Me.DesignMode Then
            Return
        End If

        _msgfilter = False

        Dim aCommand = Command()
        If aCommand <> "" Then
            Me.Show()
            Me.Opacity = 0
            Me.ShowInTaskbar = False
            System.Windows.Forms.Application.DoEvents()
            Me.Hide()
            Me.Visible = False
            System.Windows.Forms.Application.DoEvents()


            _theTwain.Init(Me.Handle)

            If (aCommand.Contains("/select")) Then
                _theTwain.Select()
                aCommand = aCommand.Replace("/select", "")
            End If

            txtPathFleName.Text = aCommand.Trim()
            If (Not _msgfilter) Then
                _msgfilter = True
                Application.AddMessageFilter(Me)
            End If
            _startTime = Now()
            _theTwain.Acquire()
        End If
    End Sub

    Public Function PreFilterMessage(ByRef m As Message) As Boolean Implements IMessageFilter.PreFilterMessage
        Dim cmd As TwainCommand = _theTwain.PassMessage(m)
        If (cmd = TwainCommand.Not) Then
            Return False
        End If

        Select Case cmd
            Case TwainCommand.CloseRequest
                EndingScan()
                _theTwain.CloseSrc()
            Case TwainCommand.CloseOk
                EndingScan()
                _theTwain.CloseSrc()
            Case TwainCommand.DeviceEvent
                Debug.Print("test")
            Case TwainCommand.TransferReady
                Dim pics As ArrayList = _theTwain.TransferPictures()
                EndingScan()
                _theTwain.CloseSrc()

                For aCounter As Integer = 1 To pics.Count Step 1
                    Dim img As IntPtr = CType(pics(aCounter - 1), IntPtr)

                    '==> save imagenya
                    Dim bmpptr = clsScanner.clsPublic.GlobalLock(img)
                    Dim pixptr = clsScanner.clsPublic.GetPixelInfo(bmpptr)
                    Dim aNamaFile = IO.Path.GetFileName(txtPathFleName.Text)
                    Dim aDirectory = IO.Path.GetDirectoryName(txtPathFleName.Text)
                    Dim aBaseName = IO.Path.GetFileNameWithoutExtension(aNamaFile)
                    Dim aExt = IO.Path.GetExtension(txtPathFleName.Text)
bikinNama:
                    Dim aPFNbaru = IO.Path.Combine(aDirectory, aBaseName & CStr(IIf(pics.Count > 1, aCounter.ToString.PadLeft(3, "0"c), "")) & aExt)
                    If IO.File.Exists(aPFNbaru) Then
                        aCounter += 1
                        GoTo bikinNama
                    End If
                    Gdip.SaveDIBAs(aPFNbaru, bmpptr, pixptr)
                    clsScanner.clsPublic.GlobalFree(bmpptr)

                Next

                
                Dim aTimespan = (Now - _startTime)
                TextBox1.AppendText(vbCrLf & "Total seconds: " & (aTimespan.TotalMilliseconds / 1000))

                If Command() <> "" Then
                    Console.WriteLine(TextBox1.Text)
                    Environment.Exit(0)
                End If

        End Select

        Return True
    End Function

    Private Sub EndingScan()
        If (_msgfilter) Then
            Application.RemoveMessageFilter(Me)
            _msgfilter = False
        End If

    End Sub

    Private _startTime As DateTime
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        _theTwain.Init(Me.Handle)

        If CheckBox1.Checked Then
            _theTwain.Select()
        End If

        If (Not _msgfilter) Then
            _msgfilter = True
            Application.AddMessageFilter(Me)
        End If
        _startTime = Now()
        _theTwain.Acquire()
    End Sub
End Class
